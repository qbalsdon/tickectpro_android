package tickets.ticketpro.com.ticketpro.Activity;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;

import com.crashlytics.android.Crashlytics;

import java.util.ArrayList;

import tickets.ticketpro.com.ticketpro.Fragment.NavigationDrawerFragment;
import tickets.ticketpro.com.ticketpro.R;
import tickets.ticketpro.com.ticketpro.View.ValidateEditText;

public class BaseActivity extends Activity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks {

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    protected NavigationDrawerFragment mNavigationDrawerFragment;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;
    private LinearLayout mContent;
    private ArrayList<ValidateEditText> requiredFields;


    protected void setmTitle(int resId){
        setTitle(getString(resId));
    }

    protected void setTitle(String title){
        mTitle = title;
        restoreActionBar();
    }

    protected void addRequiredField(ValidateEditText input){
        requiredFields.add(input);
    }

    @Override
    public void setContentView(int layoutResID) {
        getLayoutInflater().inflate(layoutResID, mContent, true);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Crashlytics.start(this);
        super.setContentView(R.layout.activity_base);

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));

        mContent = (LinearLayout)findViewById(R.id.app_content);

        requiredFields = new ArrayList<ValidateEditText>();
    }

    protected boolean validateInputs(){
        boolean valid = true;
        for (ValidateEditText input : requiredFields){
            valid &= input.isValid();
        }
        return valid;
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        // update the main content by replacing fragments

    }

    public void restoreActionBar() {
        ActionBar actionBar = getActionBar();
        if (actionBar != null) {
            actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
            actionBar.setDisplayShowTitleEnabled(true);
            actionBar.setTitle(mTitle);
        }
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);

        MenuItem logout = menu.findItem(R.id.action_logout);
        if (logout!=null)
            logout.setVisible(false);

        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.main_menu, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_login) {
            Intent intent = new Intent(this, LoginActivity.class);
            startActivityForResult(intent, LoginActivity.LOGIN_REQUEST);
        }
        if (id == R.id.action_register) {
            Intent intent = new Intent(this, CreateEditProfileActivity.class);
            intent.putExtra(CreateEditProfileActivity.PROFILE_REQUEST_TYPE, CreateEditProfileActivity.REGISTER_REQUEST);
            startActivityForResult(intent, CreateEditProfileActivity.REGISTER_REQUEST);

        }
        return super.onOptionsItemSelected(item);
    }
}