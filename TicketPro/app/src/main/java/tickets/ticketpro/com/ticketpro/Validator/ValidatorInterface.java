package tickets.ticketpro.com.ticketpro.Validator;

public interface ValidatorInterface<T> {
    public Integer isValid(T Value);
}
