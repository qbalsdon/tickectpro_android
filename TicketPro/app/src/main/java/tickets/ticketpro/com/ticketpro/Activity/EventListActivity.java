package tickets.ticketpro.com.ticketpro.Activity;

import android.app.Activity;

import android.app.ActionBar;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.support.v4.widget.DrawerLayout;
import android.widget.Spinner;

import com.crashlytics.android.Crashlytics;

import tickets.ticketpro.com.ticketpro.Adapter.EventCategoryAdapter;
import tickets.ticketpro.com.ticketpro.Fragment.NavigationDrawerFragment;
import tickets.ticketpro.com.ticketpro.R;


public class EventListActivity extends BaseActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_list);

        setTitle(R.string.upcoming_events);

        Spinner filter = (Spinner)findViewById(R.id._event_list_filter);
        filter.setAdapter(new EventCategoryAdapter(this, R.id.category_item_text));
    }
}
