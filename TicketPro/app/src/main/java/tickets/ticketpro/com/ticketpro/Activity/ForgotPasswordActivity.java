package tickets.ticketpro.com.ticketpro.Activity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import tickets.ticketpro.com.ticketpro.R;
import tickets.ticketpro.com.ticketpro.Validator.CellphoneValidator;
import tickets.ticketpro.com.ticketpro.Validator.EmailValidator;
import tickets.ticketpro.com.ticketpro.View.ValidateEditText;

public class ForgotPasswordActivity extends BaseActivity {
    public static final int FORGOT_PASSWORD_REQUEST = 2000;

    private ValidateEditText mEmailInput;
    private ValidateEditText mCellInput;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(R.string.forgot_password);
        setContentView(R.layout.activity_forgot_password);

        setupSubmitButton();

        mEmailInput = (ValidateEditText)findViewById(R.id.forgot_password_email);
        mCellInput = (ValidateEditText)findViewById(R.id.forgot_password_cell);

        addRequiredField(mEmailInput);
        addRequiredField(mCellInput);

        mEmailInput.setValidator(new EmailValidator());
        mCellInput.setValidator(new CellphoneValidator());
    }



    private void setupSubmitButton(){
        Button button = (Button)findViewById(R.id.forgot_password_submit);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validateInputs()) {
                    //TODO: start forgot password call
                    setResult(RESULT_OK);
                    finish();
                }
            }
        });
    }
}
