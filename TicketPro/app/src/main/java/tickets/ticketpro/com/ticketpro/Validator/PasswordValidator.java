package tickets.ticketpro.com.ticketpro.Validator;

import tickets.ticketpro.com.ticketpro.R;

public class PasswordValidator implements ValidatorInterface<String> {

    /**
     *
     ^                           # start-of-string
     (?=.*[0-9])                 # a digit must occur at least once
     (?=.*[a-z])                 # a lower case letter must occur at least once
     (?=.*[A-Z])                 # an upper case letter must occur at least once
     (?=.*[@#$%^&+=!\\*])        # a special character must occur at least once
     (?=\S+$)                    # no whitespace allowed in the entire string
     .{6,}                       # anything, at least six places though
     $                           # end-of-string

     * @param Value
     * @return
     */
    @Override
    public Integer isValid(String Value) {

        if (Value == null || Value.isEmpty()) return R.string.required;
        if (Value.matches("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=!\\*])(?=\\S+$).{6,}$")) {
            return null;
        } else {
            return R.string.invalid_password;
        }
    }
}
