package tickets.ticketpro.com.ticketpro.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;

import io.card.payment.CardIOActivity;
import io.card.payment.CreditCard;
import tickets.ticketpro.com.ticketpro.Adapter.GenericSpinnerAdapter;
import tickets.ticketpro.com.ticketpro.R;
import tickets.ticketpro.com.ticketpro.Validator.CreditCardValidator;
import tickets.ticketpro.com.ticketpro.Validator.LengthValidator;
import tickets.ticketpro.com.ticketpro.View.ValidateEditText;

public class PaymentActivity extends BaseActivity {
    private static final String MY_CARDIO_APP_TOKEN = "f79f2efbd8ea4e4ea204450127cd6607";
    private static final int SCAN_CARD_REQUEST = 6000;

    public static final int PAYMENT_REQUEST = 5000;

    private ValidateEditText cardname;
    private ValidateEditText cardnumber;
    private Spinner cardexpiryyear;
    private Spinner cardexpirymonth;
    private ValidateEditText cardcvv;

    private TextView event_name;
    private TextView event_date;
    private TextView event_section;
    private TextView total_payment;

    private CheckBox agreeTerms;
    private Button scanCardButton;
    private Button payButton;

    private void setupPaymentDetails(Intent intent){
        event_name.setText("Event name");
        event_date.setText("11 Dec 2014");
        event_section.setText("20 Seats, In some section");

        //TODO: calc total payment again
        total_payment.setText(String.format("%s R%.2f", getString(R.string.total_cost),6000.00d));
    }

    private TextWatcher creditCardWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            String number = cardnumber.getText().toString().replaceAll(" ", "");

            if ((number.length() - 1) % 4 == 0){
                StringBuilder builder = new StringBuilder();
                for (int k = 1; k <= number.length(); k++){
                    builder.append(number.charAt(k-1));
                    if (k % 4 == 0){
                        builder.append(" ");
                    }
                }

                String newNumber = builder.toString();
                if (!cardnumber.getText().toString().equals(newNumber)){
                    cardnumber.removeTextChangedListener(this);

                    cardnumber.setText(newNumber);
                    cardnumber.setSelection(newNumber.length());
                    cardnumber.addTextChangedListener(this);
                }
                android.util.Log.d("Blah", "Number: " + builder.toString());
            }

        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(R.string.login);
        setContentView(R.layout.activity_payment);

        event_name = (TextView)findViewById(R.id.payment_event_name);
        event_date = (TextView)findViewById(R.id.payment_event_date);
        event_section = (TextView)findViewById(R.id.payment_event_section);
        total_payment = (TextView)findViewById(R.id.total_payment);

        setupPaymentDetails(getIntent());

        cardname = (ValidateEditText)(findViewById(R.id.payment_card_name));
        cardnumber = (ValidateEditText)(findViewById(R.id.payment_card_number));
        cardcvv = (ValidateEditText)(findViewById(R.id.payment_card_cvv));

        cardexpiryyear = (Spinner)findViewById(R.id.payment_expiry_year);
        cardexpirymonth = (Spinner)findViewById(R.id.payment_expiry_month);

        setupCardSpinners();

        cardname.setValidator(new LengthValidator(3,-1));
        cardnumber.setValidator(new CreditCardValidator());
        cardcvv.setValidator(new LengthValidator(3, 4));

        agreeTerms = (CheckBox)findViewById(R.id.payment_agree);
        scanCardButton = (Button)findViewById(R.id.scan_card_button);
        payButton = (Button)findViewById(R.id.payment_submit);

        scanCardButton.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {
                Intent scanIntent = new Intent(PaymentActivity.this, CardIOActivity.class);

                // required for authentication with card.io
                scanIntent.putExtra(CardIOActivity.EXTRA_APP_TOKEN, MY_CARDIO_APP_TOKEN);

                // customize these values to suit your needs.
                scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_EXPIRY, false); // default: true

                scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_CVV, false); // default: false
                scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_POSTAL_CODE, false); // default: false

                // hides the manual entry button
                // if set, developers should provide their own manual entry mechanism in the app
                scanIntent.putExtra(CardIOActivity.EXTRA_SUPPRESS_MANUAL_ENTRY, false); // default: false

                // MY_SCAN_REQUEST_CODE is arbitrary and is only used within this activity.
                startActivityForResult(scanIntent, SCAN_CARD_REQUEST);
            }
        });

        cardnumber.addTextChangedListener(creditCardWatcher);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PaymentActivity.SCAN_CARD_REQUEST) {
            if (data != null && data.hasExtra(CardIOActivity.EXTRA_SCAN_RESULT)) {
                CreditCard scanResult = data.getParcelableExtra(CardIOActivity.EXTRA_SCAN_RESULT);

                // Never log a raw card number. Avoid displaying it, but if necessary use getFormattedCardNumber()
                cardnumber.setText(scanResult.getFormattedCardNumber());
                cardnumber.requestFocus();
            }
            return;
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    private void setupCardSpinners(){
        ArrayList<String> months = new ArrayList<String>(Arrays.asList(getResources().getStringArray(R.array.months)));

        cardexpirymonth.setAdapter(new GenericSpinnerAdapter<String>(this, R.layout.generic_spinner_item, months, new GenericSpinnerAdapter.SetupViewInterface<String>() {
            @Override
            public void setupView(String item, View construction) {
                TextView view = (TextView)construction.findViewById(R.id.item_text);
                view.setText(item);
            }
        }));

        Calendar c = Calendar.getInstance();
        ArrayList<String> years = new ArrayList<String>();
        int currentYear = c.get(Calendar.YEAR);
        currentYear = currentYear - ((currentYear / 100) * 100);
        for (int i = 0; i < 10; i++){
            years.add(String.format("%02d",currentYear + i));
        }

        cardexpiryyear.setAdapter(new GenericSpinnerAdapter<String>(this, R.layout.generic_spinner_item, years, new GenericSpinnerAdapter.SetupViewInterface<String>() {
            @Override
            public void setupView(String item, View construction) {
                TextView view = (TextView)construction.findViewById(R.id.item_text);
                view.setText(item);
            }
        }));
    }
}
