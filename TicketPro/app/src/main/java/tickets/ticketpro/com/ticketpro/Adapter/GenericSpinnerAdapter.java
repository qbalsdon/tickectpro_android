package tickets.ticketpro.com.ticketpro.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import tickets.ticketpro.com.ticketpro.R;

public class GenericSpinnerAdapter <T> extends ArrayAdapter<T> {

    public interface  SetupViewInterface <T>{
        public void setupView(T item, View construction);
    }

    private int mLayoutId;

    private ArrayList<T> mObjects;

    private SetupViewInterface mSetupEvent;

    public GenericSpinnerAdapter(Context context, int resource, ArrayList<T> objects, SetupViewInterface setupViewEvent) {
        super(context, resource);

        mLayoutId = resource;
        mObjects = objects;
        mSetupEvent = setupViewEvent;
    }

    @Override
    public int getCount() {
        return mObjects.size();
    }

    @Override public View getDropDownView(int position, View convertView, ViewGroup parent){
        return constructView(position, convertView, parent, true);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return constructView(position, convertView, parent, false);
    }

    private View constructView(int position, View convertView, ViewGroup parent, boolean isDropDown){
        if (convertView == null){
            convertView = ((LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(mLayoutId, null, false);
        }

        if (isDropDown){
            convertView.setBackgroundColor(getContext().getResources().getColor(R.color.spinner_back));
        }

        mSetupEvent.setupView(mObjects.get(position), convertView);

        return convertView;
    }

    @Override
    public int getPosition(T item) {
        return mObjects.indexOf(item);
    }

    @Override
    public T getItem(int position) {
        return mObjects.get(position);
    }
}
