package tickets.ticketpro.com.ticketpro.Activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Map;

import tickets.ticketpro.com.ticketpro.Adapter.EventCategoryAdapter;
import tickets.ticketpro.com.ticketpro.Adapter.GenericSpinnerAdapter;
import tickets.ticketpro.com.ticketpro.R;


public class BookEventActivity extends BaseActivity implements View.OnClickListener{

    //In and out
    public static final String EVENT = "event_object";
    public static final String DATE = "event_date";

    //out
    public static final String SEAT_COUNT = "event_num_seats";
    public static final String SECTION_ID = "event_section_id";

    private Spinner mSectionSpinner;
    private Spinner mSeatSpinner;
    private TextView mEventDescription;
    private Button mBookButton;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_event);

        setTitle(R.string.booking);

        mSectionSpinner = (Spinner)findViewById(R.id.section_spinner);
        mSeatSpinner = (Spinner)findViewById(R.id.seats_spinner);
        mEventDescription = (TextView)findViewById(R.id.event_description);
        mBookButton = (Button)findViewById(R.id.book_now_submit);

        mEventDescription.setText("Enjoy the day with us and many top brands in SA. The celebration of Joburg!");
        populateSpinners();

        mBookButton.setOnClickListener(this);
    }

    private void populateSpinners(){

        //TODO: remove test data
        ArrayList<Map.Entry<Double, String>> sections = new ArrayList<Map.Entry<Double, String>>();
        String[] sectionList = new String[] {"R150 General Access","R200 Special Access","R300.00 Super special","R25.00 Pig door"};
        Double[] priceList = new Double[] {200.00, 200.00, 300.00, 50.00};
        for (int k = 0; k < sectionList.length; k++){
            AbstractMap.SimpleEntry<Double, String> p = new AbstractMap.SimpleEntry<Double, String>(priceList[k], sectionList[k]);
            sections.add(p);
        }

        ArrayList<Integer> seats = new ArrayList<Integer>();

        Integer[] seatsList = new Integer[] {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20};
        for (int k = 0; k < seatsList.length; k++){
            seats.add(seatsList[k]);
        }
        //---------------
        mSectionSpinner.setAdapter(new GenericSpinnerAdapter<Map.Entry<Double, String>>(
                this,
                R.layout.generic_spinner_item,
                sections,
                new GenericSpinnerAdapter.SetupViewInterface<Map.Entry<Double, String>>() {

                    @Override
                    public void setupView(Map.Entry<Double, String> item, View construction) {
                        TextView text = (TextView)construction.findViewById(R.id.item_text);
                        text.setText(item.getValue());
                        text.setTextColor(getResources().getColor(android.R.color.white));
                        text.setTag(item.getKey());
                    }
                }
        ));

        mSeatSpinner.setAdapter(new GenericSpinnerAdapter<Integer>(
                this,
                R.layout.generic_spinner_item,
                seats,
                new GenericSpinnerAdapter.SetupViewInterface<Integer>() {

                    @Override
                    public void setupView(Integer item, View construction) {
                        TextView text = (TextView)construction.findViewById(R.id.item_text);
                        text.setText(String.valueOf(item));
                        text.setTextColor(getResources().getColor(android.R.color.white));
                        text.setTag(item);
                    }
                }
        ));
    }

    @Override
    public void onClick(View view) {
        //TODO: Check login. if not logged in, do an activity result and then start the payment process

        preProceed();
    }

    private void preProceed(){
        mSeatSpinner.getAdapter().getItem(mSeatSpinner.getSelectedItemPosition());
        int numSeats = (Integer)mSeatSpinner.getSelectedItem();
        Map.Entry<Double, String> selectedArea = (Map.Entry<Double, String>)mSectionSpinner.getSelectedItem();

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View view = ((LayoutInflater)getSystemService(LAYOUT_INFLATER_SERVICE)).inflate(R.layout.dialog_confirm_booking, null, false);

        TextView textDate = (TextView)view.findViewById(R.id.dialog_event_date);
        TextView textName = (TextView)view.findViewById(R.id.dialog_event_name);
        TextView textPrice = (TextView)view.findViewById(R.id.dialog_event_price);
        TextView textSeatSection = (TextView)view.findViewById(R.id.dialog_event_seats_and_section);

        textName.setText("Event name");
        textDate.setText("11 Dec 2014");
        textPrice.setText(String.format("%s R%.2f", getString(R.string.total_cost),numSeats * selectedArea.getKey()));
        textSeatSection.setText(String.format("%d %s, %s", numSeats, getString(R.string.seats), selectedArea.getValue()));

        builder.setView(view);

        builder.setPositiveButton(R.string.check_out, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent(BookEventActivity.this, PaymentActivity.class);
                //TODO: Add event details
                startActivityForResult(intent, PaymentActivity.PAYMENT_REQUEST);
            }
        });

        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });

        builder.create().show();
    }
}
