package tickets.ticketpro.com.ticketpro.Activity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import tickets.ticketpro.com.ticketpro.R;
import tickets.ticketpro.com.ticketpro.Validator.CellphoneValidator;
import tickets.ticketpro.com.ticketpro.Validator.ConfirmPasswordValidator;
import tickets.ticketpro.com.ticketpro.Validator.EmailValidator;
import tickets.ticketpro.com.ticketpro.Validator.LengthValidator;
import tickets.ticketpro.com.ticketpro.Validator.PasswordValidator;
import tickets.ticketpro.com.ticketpro.View.ValidateEditText;

public class CreateEditProfileActivity extends BaseActivity {
    public static final int REGISTER_REQUEST = 3000;
    public static final int EDIT_PROFILE_REQUEST = 4000;

    public static final String PROFILE_REQUEST_TYPE = "PROFILE_REQUEST_TYPE";


    private ValidateEditText mUsername;
    private ValidateEditText mPassword;
    private ValidateEditText mRepeatPassword;
    private ValidateEditText mFirstName;
    private ValidateEditText mLastName;
    private ValidateEditText mEmailInput;
    private ValidateEditText mCellInput;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(R.string.forgot_password);
        setContentView(R.layout.activity_register);

        setupSubmitButton();

        mUsername = (ValidateEditText)findViewById(R.id.register_username);
        mPassword = (ValidateEditText)findViewById(R.id.register_password);
        mRepeatPassword = (ValidateEditText)findViewById(R.id.register_repeat_password);
        mFirstName = (ValidateEditText)findViewById(R.id.register_firstname);
        mLastName = (ValidateEditText)findViewById(R.id.register_lastname);

        mEmailInput = (ValidateEditText)findViewById(R.id.register_email);
        mCellInput = (ValidateEditText)findViewById(R.id.register_cell);

        if (getIntent().hasExtra(PROFILE_REQUEST_TYPE)
                && getIntent().getIntExtra(PROFILE_REQUEST_TYPE, REGISTER_REQUEST) == EDIT_PROFILE_REQUEST){
            setupProfileForEditing();
        }

        mUsername.setValidator(new LengthValidator(6,-1));
        mPassword.setValidator(new PasswordValidator());
        mRepeatPassword.setValidator(new ConfirmPasswordValidator(new ConfirmPasswordValidator.ValueRetriever() {
            @Override
            public String getValue() {
                return mPassword.getText().toString();
            }

        }));
        mPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                mRepeatPassword.setError(null);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        mFirstName.setValidator(new LengthValidator(2, -1));
        mLastName.setValidator(new LengthValidator(2, -1));

        mEmailInput.setValidator(new EmailValidator());
        mCellInput.setValidator(new CellphoneValidator());

        addRequiredField(mUsername);
        addRequiredField(mPassword);
        addRequiredField(mRepeatPassword);
        addRequiredField(mFirstName);
        addRequiredField(mLastName);
        addRequiredField(mEmailInput);
        addRequiredField(mCellInput);

    }

    private void setupProfileForEditing(){

    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem login = menu.findItem(R.id.action_register);
        if (login!=null)
            login.setVisible(false);

        return super.onPrepareOptionsMenu(menu);
    }

    private void setupSubmitButton(){
        Button button = (Button)findViewById(R.id.register_submit);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validateInputs()) {

                    setResult(RESULT_OK);
                    finish();
                }
            }
        });
    }
}
