package tickets.ticketpro.com.ticketpro.Validator;

import tickets.ticketpro.com.ticketpro.R;

public class CreditCardValidator implements ValidatorInterface<String> {

    @Override
    public Integer isValid(String Value) {

        if (Value == null || Value.isEmpty()) return R.string.required;

        String card = Value.replace("-","").replace(" ","").trim();

        if (card.isEmpty()) return R.string.required;

        if (card.matches("^4[0-9]{12}(?:[0-9]{3})?$") || card.matches("^5[1-5][0-9]{14}$")){
            return null;
        } else {
            return R.string.invalid_credit_card;
        }
    }
}
