package tickets.ticketpro.com.ticketpro.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import tickets.ticketpro.com.ticketpro.R;

public class EventCategoryAdapter extends ArrayAdapter<String> {

    private String[] mObjects;
    private Context mContext;

    public EventCategoryAdapter(Context context, int resource) {
        super(context, resource);


        mObjects = context.getResources().getStringArray(R.array.event_filter);
    }

    @Override
    public int getCount() {
        return mObjects.length;
    }

    @Override public View getDropDownView(int position, View convertView, ViewGroup parent){
        return constructView(position, convertView, parent, true);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return constructView(position, convertView, parent, false);
    }

    private View constructView(int position, View convertView, ViewGroup parent, boolean isDropDown){
        if (convertView == null){
            convertView = ((LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.category_item,null,false);
        }

        TextView label = (TextView)convertView.findViewById(R.id.category_item_text);
        ImageView image = (ImageView)convertView.findViewById(R.id.category_item_image);

        label.setText(mObjects[position]);

        if (isDropDown){
            convertView.setBackgroundColor(getContext().getResources().getColor(R.color.spinner_back));
        }

        if (mObjects[position].equals(getContext().getString(R.string.order_by_time))){
            image.setImageDrawable(getContext().getResources().getDrawable(android.R.drawable.ic_menu_recent_history));
        }
        if (mObjects[position].equals(getContext().getString(R.string.order_by_category))){
            image.setImageDrawable(getContext().getResources().getDrawable(android.R.drawable.ic_menu_today));
        }
        if (mObjects[position].equals(getContext().getString(R.string.order_by_location))){
            image.setImageDrawable(getContext().getResources().getDrawable(android.R.drawable.ic_menu_mylocation));
        }
        return convertView;
    }

    @Override
    public int getPosition(String item) {
        for (int i = 0; i < mObjects.length; i++){
            if (mObjects[i].equals(item)){
                return i;
            }
        }

        return -1;
    }
}
