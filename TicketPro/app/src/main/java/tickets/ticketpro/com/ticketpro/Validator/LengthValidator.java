package tickets.ticketpro.com.ticketpro.Validator;

import tickets.ticketpro.com.ticketpro.R;

public class LengthValidator implements ValidatorInterface<String> {

    private int mMinLength, mMaxLength;

    public LengthValidator(int minLength, int maxLength){
        mMinLength = minLength;
        mMaxLength = maxLength;
    }

    @Override
    public Integer isValid(String Value) {

        if (Value == null || Value.isEmpty()) return R.string.required;
        if (mMinLength >= 0 && Value.length() >= mMinLength){
            if (mMaxLength >= 0 && Value.length() >= mMaxLength){
                return R.string.value_too_long;
            } else {
                return null;
            }
        } else {
            return R.string.value_too_short;
        }
    }
}
