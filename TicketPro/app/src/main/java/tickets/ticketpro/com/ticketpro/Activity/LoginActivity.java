package tickets.ticketpro.com.ticketpro.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import tickets.ticketpro.com.ticketpro.R;
import tickets.ticketpro.com.ticketpro.Validator.LengthValidator;
import tickets.ticketpro.com.ticketpro.View.ValidateEditText;

public class LoginActivity extends BaseActivity {

    public static final int LOGIN_REQUEST = 1000;

    private ValidateEditText username;
    private ValidateEditText password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(R.string.login);
        setContentView(R.layout.activity_login);

        username = (ValidateEditText)(findViewById(R.id.login_username));
        password = (ValidateEditText)(findViewById(R.id.login_username));

        username.setValidator(new LengthValidator(6,-1));
        password.setValidator(new LengthValidator(6,-1));

        setupForgotPasswordLink();

        setupLoginButton();
    }

    private void setupLoginButton() {
        Button submit = (Button)findViewById(R.id.login_submit);
        submit.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {
                if (LoginActivity.this.validateInputs()){
                    //TODO: Implement login logic

                    setResult(RESULT_OK);
                    finish();
                }
            }
        });
    }

    private void setupForgotPasswordLink() {
        TextView forgotPassword = (TextView)findViewById(R.id.forgot_password_link);
        forgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this, ForgotPasswordActivity.class);
                startActivityForResult(intent, ForgotPasswordActivity.FORGOT_PASSWORD_REQUEST);
            }
        });
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem login = menu.findItem(R.id.action_login);
        if (login!=null)
            login.setVisible(false);

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == ForgotPasswordActivity.FORGOT_PASSWORD_REQUEST && resultCode == RESULT_OK){
            this.finish();
        }

        super.onActivityResult(requestCode, resultCode, data);
    }
}
