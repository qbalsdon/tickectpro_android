package tickets.ticketpro.com.ticketpro.Validator;

import tickets.ticketpro.com.ticketpro.R;

public class CellphoneValidator implements ValidatorInterface<String> {
    @Override
    public Integer isValid(String Value) {

        if (Value == null || Value.isEmpty()) return R.string.required;
        if (Value.matches("[0][1-9][0-9]{8}$")){
            return null;
        } else {
            return R.string.invalid_cell_phone;
        }
    }
}
