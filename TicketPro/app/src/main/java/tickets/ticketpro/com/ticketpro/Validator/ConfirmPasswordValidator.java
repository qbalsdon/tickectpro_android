package tickets.ticketpro.com.ticketpro.Validator;

import android.widget.EditText;

import tickets.ticketpro.com.ticketpro.R;

public class ConfirmPasswordValidator implements ValidatorInterface<String>{
    public interface ValueRetriever{
        public String getValue();
    }

    private ValueRetriever mValueRetriever;


    public ConfirmPasswordValidator(ValueRetriever retriever){
        mValueRetriever = retriever;
    }

    @Override
    public Integer isValid(String Value) {

        if (mValueRetriever != null && !mValueRetriever.getValue().equals(Value)){
            return R.string.values_are_not_the_same;
        }
        return null;
    }
}
